//node.js
//9999999999999999???

// [sign(1) | exp(8) | mantissa(23)] 
// -126 <= exp-127 <= 127
// NaN - exp=255, mantissa!=0
// Infinity - exp=255, mantissa=0

log2 = (n)=>Math.log(n)/Math.log(2);
pow2 = (n)=>Math.pow(2, n);
_hbound = (2 - pow2(-23)) * pow2(127); // 0 11111110 [23*1]
_nbound = pow2(-126);
_lbound = pow2(-149);

_Nan           = '01111111100000000000000000000001'; // one of NaNs..
_positive_inf  = '01111111100000000000000000000000';
_negative_inf  = '11111111100000000000000000000000';
_positive_zero = '00000000000000000000000000000000';
_negative_zero = '10000000000000000000000000000000';

function construct_string(sign, exp, binary){
    var expstr = (exp+127).toString(2);
    for(var i=8-expstr.length;i>0;i--)
        expstr = 0+expstr;
    for(var i=23-binary.length;i>0;i--)
        binary+=0;
    return sign+expstr+binary;
}

function get_float32_repr(query) {
    if (query==='-0') return _negative_zero;
    else if (query == 0) return _positive_zero;
    var num = query-0;
    if (isNaN(num)) return _Nan;
    if (num>_hbound) return _positive_inf;
    else if (-num>_hbound) return _negative_inf;
    var sign = num < 0 ? (num=-num, 1) : 0;
    var binary = num.toString(2).replace('.','');
    if (num<_nbound) 
        return construct_string(sign, -127, binary.slice(127,127+23))
    var exp = Math.floor(log2(num));
    var indexOf1 = binary.indexOf(1)+1;
    var args = [sign, exp, binary.slice(indexOf1,indexOf1+23)];
    return construct_string.apply(null, args);
}

function test_single(input, expected_output){
    var output = get_float32_repr(input);
    if (output!=expected_output)
        console.log('>FAIL! '+input+': "'+output+'" instead of "'+expected_output+'"');
    else 
        console.log('ok, '+input+': "'+expected_output+'"');
}

function run_tests(){
    test_single('13', '01000001010100000000000000000000');
    test_single(-6.75, '11000000110110000000000000000000');
    test_single(0.15625, '00111110001000000000000000000000');
    test_single(1, '00111111100000000000000000000000');
    test_single(0.2, '00111110010011001100110011001100');
    test_single(Math.pow(2, -23), '00110100000000000000000000000000');
    // hex?
    test_single('0xD', '01000001010100000000000000000000'); 
    //_test_single('-0xD', '11000001010100000000000000000000');  not working
    test_single('0xXXL', _Nan);
    test_single('cafebabe', _Nan);
    test_single('deadbeef', _Nan);
    // good number?
    test_single('xD', _Nan);
    test_single('1s1', _Nan);
    test_single('1s', _Nan);
    test_single(0, _positive_zero); 
    test_single('0', _positive_zero); 
    test_single('-0', _negative_zero);
    // unsupported size
    test_single(1e+40, _positive_inf); 
    test_single('1e+2007', _positive_inf);
    test_single('-1e+40', _negative_inf);
    test_single(pow2(-150), _positive_zero);
    test_single(-pow2(-150), _negative_zero);
    // denormalized
    test_single(pow2(-126)*(1-pow2(-23)), '00000000011111111111111111111111');
    test_single(pow2(-149), '00000000000000000000000000000001');
    test_single(pow2(-127), '00000000010000000000000000000000');
    test_single(-pow2(-127), '10000000010000000000000000000000');
    test_single(pow2(-127)+pow2(-149), '00000000010000000000000000000001');
    test_single(pow2(-127)-pow2(-149), '00000000001111111111111111111111');
}

function main(args) {
    if (!args[2]){
        console.log('\tusage: node ieee754.js [number]');
        console.log('\tbtw im running tests...\n');
        run_tests();
        return 1;
    }
    var repr = get_float32_repr(args[2]);
    console.log(repr[0]+' '+repr.slice(1,9)+' '+repr.slice(9,32));
    return 0;
}

exports = module.exports;
exports.float32 = get_float32_repr;

if (require.main === module) 
    process.exit(main(process.argv));
