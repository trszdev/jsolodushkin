//node.js

function repeat(elem, times) {
    return Array(times+1).join(0).split('').map(a=>elem);
}

function count_entropy(query) {
    var symbol_range = Math.pow(2, 16);
    var letter_frequency = repeat(0, symbol_range);
    var uniq_letters = 0;
    for(var i=0;i<query.length;i++){
        var count = ++letter_frequency[query.charCodeAt(i)];
        if (count==1) ++uniq_letters;
    }
    var base = arguments.length==1?uniq_letters:arguments[1];
    var log = (n)=>Math.log(n)/Math.log(base);
    var result = 0;
    for(var i=0;i<symbol_range;i++) {
        var freq = letter_frequency[i] / query.length;
        if (freq>0) result += freq*log(freq);
    }
    return 0-result;
}

function main(args) {
    if (args.length!=3) {
        console.log('\tusage: node entropy.js [query]');
        return 1;
    }
    console.log(count_entropy(args[2]));
    return 0;
}

exports = module.exports;
exports.count_entropy = count_entropy;

if (require.main === module) 
    process.exit(main(process.argv));
