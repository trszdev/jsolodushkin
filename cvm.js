//node.js

Engine = function() {
    var self = this;
    self.version = '2.2.8';
    self.memory = {};
    self.set_var = function(name, expr) {
        self.memory[name] = self.eval_expr(expr);
    }

    self.get_var = function(name) {
        if (name in self.memory) 
            return self.memory[name];
        var parsed = name-0;
        if (!isNaN(parsed)) return parsed;
        throw 'No such variable "'+name+'"';
    }
    
    var operator_prior = function(op){
        switch(op){
            case '^':
            case '%':
                return 4;
            case '+':
            case '-':
                return 2;
            case '*':
            case '/':
                return 3;
            case '(':
            case ')':
                return 1;
            default:
                return 0;
        }
    }

    // http://algolist.manual.ru/syntax/revpn.php
    // no bracket support
    var polish = function(expr){
        var result = '';
        var stack = [];
        var operator_regex = /[\^\+\-\(\)\/\*\%]/g;
        var tokens = expr
            .replace(operator_regex, (p, i) => ' '+p+' ')
            .trim()
            .split(' ')
            .forEach((token) => {
                if (!operator_regex.test(token))
                    result += token + ' ';
                else {
                    if (stack.length != 0){
                        var p1 = operator_prior(token);
                        if (operator_prior(stack[stack.length-1]) >= p1) 
                            result += stack.pop() + ' ';
                    }
                    stack.push(token);
                }    
        });
        result += stack.reverse().join(' ');
        console.log(result);
        return result.trim();
        //return '1 2 + 4 * 3 +'; // example
    }

    // 1) ������� � �������� �������
    // 2) ���������� ������ ����� ����
    // ��� �� ���������
    self.eval_expr = function(expr) {
        var stack = [];
        polish(expr).split(' ').forEach((token) => {
            if ('+*-/^%'.split('').indexOf(token)==-1)
                stack.push(self.get_var(token));
            else 
                var b = stack.pop(), a = stack.pop();
                switch(token){
                    case '+': stack.push(a+b); break;
                    case '-': stack.push(a-b); break;
                    case '*': stack.push(a*b); break;
                    case '/': stack.push(a/b); break;
                    case '^': stack.push(Math.pow(a,b)); break;
                    case '%': stack.push(a%b); break;
                }
        });
        return stack.pop();
    }
}

Interpret = function(engine) {
    var self = this; // gr8 
    self.engine = engine;
    self.greet = function() {
        console.log("Hello, this is counting virtual machine.");
        console.log("Engine version: " + engine.version);
    }
    
    self.input = function(input){
        input.replace(/ /g,'').split(';').forEach(function(cmd){
            if (cmd=='') return;
            if (['exit','quit','leave'].indexOf(cmd)!=-1)
                self.onexit();
            else if (cmd=='debug')
                console.log(engine.memory);
            else if (cmd=='help')
                self.onhelp();
            else {
                if (cmd.indexOf('=') == -1){
                    console.log(engine.eval_expr(cmd));
                }
                else {
                    var eq = cmd.split('=', 2);
                    engine.set_var(eq[0], eq[1]);
                }
            }
        });
    }

    self.hint = function(){
        console.log('Hint: type "help" or "exit"');
    }

    self.onexit = function(){
        console.log('c u l8r');
        process.exit(0);
    }

    self.onhelp = function(){
        console.log('i wont help you.');
    }
}

if (require.main==module) {
    var engine = new Engine();
    if (process.argv[2]) {
        console.log(engine.eval_expr(process.argv[2]));
        process.exit(0);
    }
    var interpret = new Interpret(engine);
    var con = require('readline').createInterface(process.stdin, process.stdout);
    interpret.greet();
    con.setPrompt('>>> ');
    con.prompt();
    con.on('line', function(line){
        try {
            interpret.input(line);
        }
        catch(err) { 
            console.log('Bad input: ' + err);
            interpret.hint();
        }
        con.prompt();
    });
}
