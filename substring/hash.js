//node.js 

module.exports.rabinkarp = {};
module.exports.squarehash = {};

function sqr(x){
    return x*x;
}


function ord(s){
    return s.charCodeAt(0);
}


function is_goodcase(query, substring, callback){
    if (query.length < substring.length) return false;
    if (substring.length==0) {
        for(var i=0;i<query.length;i++)
            callback(i);
        return false;
    }
    return true;
}

function are_equal(query, substring, qindex){
    for(var j=0;j<substring.length;j++){
        if (query[qindex+j]!=substring[j]) 
                    return false;
    }
    return true;
}

// still has a possibility to give a wrong answer if strings will be huge
module.exports.findall = function(query, substring, callback){
    if (!is_goodcase(query, substring, callback)) return;
    var hpattern = 0;
    var htext = 0;
    for(var i=0;i<substring.length;i++) {
        hpattern += ord(substring[i]);
        htext += ord(query[i]);
    }
    var length = query.length - substring.length;
    for(var i=0;i<length;i++){
        if (htext==hpattern && are_equal(query, substring, i))
            callback(i);
        htext -= ord(query[i]);
        htext += ord(query[i+substring.length]);
    }
    if (htext==hpattern && are_equal(query, substring, i))
        callback(i);
}


// same
module.exports.squarehash.findall = function(query, substring, callback){
   if (!is_goodcase(query, substring, callback)) return;
    var hpattern = 0;
    var htext = 0;
    for(var i=0;i<substring.length;i++) {
        hpattern += sqr(ord(substring[i]));
        htext += sqr(ord(query[i]));
    }
    var length = query.length - substring.length;
    for(var i=0;i<length;i++){
        if (htext==hpattern && are_equal(query, substring, i))
            callback(i);
        htext -= sqr(ord(query[i]));
        htext += sqr(ord(query[i+substring.length]));
    }
    if (htext==hpattern && are_equal(query, substring, i))
        callback(i);
}


MOD = 129; // some number to avoid overflow
MOD2 = MOD * 65536;
module.exports.rabinkarp.findall = 
function(query, substring, callback){
    if (!is_goodcase(query, substring, callback)) return;
    var hpattern = 0;
    var htext = 0;
    var length = query.length - substring.length;
    var maxpow = 1;
    for(var i=0; i<substring.length; i++) {
        hpattern *= 2;
        htext *= 2;
        maxpow = (maxpow*2) % MOD;
        hpattern += ord(substring[i]);
        hpattern %= MOD;
        htext += ord(query[i]);
        htext %= MOD;
    }
    for(var i=0; i<length; i++){
        if (htext==hpattern && are_equal(query, substring, i))
            callback(i);
        htext *= 2;
        htext += MOD2 - ord(query[i])*maxpow;
        htext += ord(query[i+substring.length]);
        htext %= MOD;
    }
    if (htext==hpattern && are_equal(query, substring, i))
        callback(i);
}
