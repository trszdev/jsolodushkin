//node.js 
// TODO: too slow

function transitions_table(query){
    var alpha = {};
    var result = Array(query.length+1);
    for(var i=0; i<=query.length; i++) 
        result[i] = {};
    result[0][query[0]] = 0;
    for(var i=0; i<query.length; i++) {
        var prev = result[i][query[i]] || 0;
        alpha[query[i]] = 1;
        result[i][query[i]] = i+1;
        for(var letter in alpha)
            result[i+1][letter] = result[prev][letter];
    }
    return result;
}


module.exports.findall = function(query, substring, callback){
    if (substring.length == 0){
        for(var i=0;i<query.length;i++)
            callback(i);
        return;
    }
    var table = transitions_table(substring);
    var termstate = table.length-1;
    var slen = substring.length;
    var current_state = 0;
    for(var i=0; i<query.length;){
        var symb = query[i++];
        current_state = table[current_state][symb] || 0;
        if (current_state==termstate) callback(i-slen);
    }
}
