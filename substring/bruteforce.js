//node.js

function findall(query, substring, callback){
    for(var i=0;i<query.length;i++){
        var found = true;
        for(var j=0;j<substring.length;j++){
            if (substring[j]!=query[i+j]) {
                found = false;
                break;
            }
        }
        if (found) callback(i);
    }
}

module.exports.findall = findall;
