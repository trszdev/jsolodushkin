testmethods = [
    {name:'boyer-moore', method: require('./boyer-moore').findall}, 
    {name:'boyer-moore-simplified', method: require('./boyer-moore').simplified.findall}, 
    {name:'rabinkarp-base-2', method: require('./hash').rabinkarp.findall},
    {name:'sumhash', method: require('./hash').findall}, 
    {name:'squaresumhash', method: require('./hash').squarehash.findall}, 
    {name:'aho-corasick', method: require('./aho-corasick').findall},
    {name:'bruteforce', method: require('./bruteforce').findall}, 
]

CORRECTNESS_ONLY = hasoption('--co');
EFFICIENCY_ONLY = hasoption('--eo');
VERBOSE = hasoption('--verbose');





fs = require('fs');

function hasoption(opt){
    return process.argv.indexOf(opt) != -1;
}

function info(message) {
    console.log('> '+message);
}


function msg(message) {
    if (!VERBOSE) return;
    console.log(message);
}


function repeat(elem, count){
    return Array(count+1).join(0).split('').map(x=>elem);
}


function timestamp(){
    return new Date;
}

function less(text, amount){
    text = text.replace(/[\n\r]/g, '.');
    return  text.length>amount ? 
        text.substring(0, amount)+'...' : text;
}


function random_int(min, max) {
  var rand = min + Math.random() * (max - min)
  rand = Math.round(rand);
  return rand;
}


function random_testcase(textlength, patternlength){
    var alphabet = 'abcdefghijklmnopqrstuvwxyz';
    var alen = alphabet.length;
    var text = '';
    for(var i=0;i<textlength;i++)
        text += alphabet[random_int(0, alen-1)];
    return { text:text, pattern:text.substr(0, patternlength) };
}


_txts = {};

function readtxt(filename){
    return _txts[filename] || (_txts[filename]=fs.readFileSync(filename, 'utf8'));
}

function test(method, text, pattern){
    var indeces = [];
    var start = timestamp();
    method(text, pattern, a=>indeces.push(a));
    return {
        elapsed: timestamp() - start,
        indeces: indeces
    };
}

function chr(code){
    return String.fromCharCode(code);
}

_maxch = 1<<16;
_tcase1 = 'a'.repeat(10000);
_tcase2 = 'abc'.repeat(10000);
_tcase3 = chr(_maxch).repeat(100) + chr(_maxch-1).repeat(100);
_tcase4 = '';

for(var i=0;i<256;i++) _tcase4 += chr(i);


test_cases = [
    // common tests
    {text:'aaaa', pattern:'a', expected_indeces:[0,1,2,3]},
    {text:'aaaa', pattern:'b', expected_indeces:[]},
    {text:'abcdef', pattern:'xyz', expected_indeces:[]},
    {text:'abcdxeerty', pattern:'xyz', expected_indeces:[]},
    {text:'abcdxeertyz', pattern:'xyz', expected_indeces:[]},
    {text:'aaaaabaaaaa', pattern:'ab', expected_indeces:[4]},
    {text:'cabcabbac', pattern:'abc', expected_indeces:[1]},
    {text:'abcdeedcba', pattern:'', expected_indeces:[0,1,2,3,4,5,6,7,8,9]},
    {text:'He11oworld', pattern:'world', expected_indeces:[5]},
    {text:'Мама мыла раму мылараму', pattern:'мыла', expected_indeces:[5,15]},
    {text:'Мама мыла раму мылараму', pattern:'мама', expected_indeces:[]},
    {text:'a', pattern:'a', expected_indeces:[0]},
    {text:'a', pattern:'aa', expected_indeces:[]},
    // possible overflow
    {text:'def'+_tcase2+'fre', pattern: 'fed', expected_indeces:[]},
    {text:'def'+_tcase2+'fre', pattern: _tcase2, expected_indeces:[3]},
    {text:'tyasi'+_tcase1+'dce', pattern: _tcase1, expected_indeces:[5]},
    {text:'ab'.repeat(10000), pattern: _tcase1, expected_indeces:[]},
    {text:_tcase3+_tcase1+_tcase3, pattern: _tcase3, expected_indeces:[0,10200]},
    {text:chr(_maxch).repeat(30000), pattern: _tcase3, expected_indeces:[]},
    {text:'abc'+(chr(_maxch-1)+chr(_maxch-2)+chr(_maxch-3)+chr(_maxch-4)).repeat(30000)+'abc', pattern: 'abc', expected_indeces:[0,120003]},
    {text:_tcase4.repeat(4), pattern: _tcase4, expected_indeces:[0,256,512,768]},
    // heuristic troubles
    {text:'worldwroldwlordworldwworlddlorw', pattern:'world', expected_indeces:[0, 15, 21]},
    {text:'ananananananan', pattern:'ananan', expected_indeces:[0,2,4,6,8]},
    {text:'колокохколаколколоколокол', pattern:'колокол', expected_indeces:[14, 18]},
    {text:'dabdbabdabc', pattern:'dabababd', expected_indeces:[]},
    {text:'dabababddabadabd', pattern:'dabababd', expected_indeces:[0]},
    {text:'bdbddbdbdbdabdbd', pattern:'bdbdabd', expected_indeces:[7]},
    {text:'abcdefghij', pattern:'abcdefghij', expected_indeces:[0]},
]



test_cases2 = [
    //random_testcase(3000, 30),
    //random_testcase(1000, 10000),
    //random_testcase(10000, 10000),
    random_testcase(100000, 10000),
    test_cases[16], 
    test_cases[17], 
    test_cases[18], 
    test_cases[19], 
    test_cases[20], 
    { text: readtxt('Война и мир.txt'), pattern: 'Болкон' },
    { text: readtxt('Война и мир.txt'), pattern: readtxt('Война и мир.txt').substr(0, 300) },
    { text: readtxt('Война и мир.txt'), pattern: 'Он подошел к Анне Павловне, поцеловал ее руку, подставив ей свою надушенную и сияющую лысину, и покойно уселся на диване' },
    { text: readtxt('long_exotic'), pattern: readtxt('exotic.msg') },
    { text: readtxt('long_exotic'), pattern: '!!!!!!!!!!!!!!!!!!' },
]


function test_correctness(){
    info('Testing correctness...');
    function print_error(name, i, tcase, got){
        info('['+name+'] wrong at '+i+'th test');
        msg('Text:    "'+less(tcase.text, 45)+'" ('+tcase.text.length+' chars)');
        msg('Pattern: "'+less(tcase.pattern, 45)+'" ('+tcase.pattern.length+' chars)');
        msg('Expected: ['+tcase.expected_indeces+'], but got: ['+got+']');
    }
    for(var j=0;j<testmethods.length;j++){
        var method = testmethods[j];
        var success = true;
        //msg('Testing '+method.name);
        for(var i=0;i<test_cases.length;i++){
            var tcase = test_cases[i];
            var text = tcase.text, pattern = tcase.pattern, expected = tcase.expected_indeces;
            var time = test(method.method, text, pattern);
            var got = time.indeces; time = time.elapsed;
            success = got.length == expected.length;
            for(var k=0;k<got.length;k++){
                if (got[k]!=expected[k]){
                    success = false;
                    break;
                }
            }
            if (!success) { 
                print_error(method.name, i, tcase, got);
                break;
            }
            //msg((i+1) + 'th test passed ('+time+' ms)');
        }
        if (success) info('['+ method.name +'] ok');
    }
}


function test_efficiency(){
    info('Testing efficiency...');
    var stats = {};
    for(var i=0;i<testmethods.length;i++)
        stats[testmethods[i].name] = [0, 99999999, -1];
    for(var i=0;i<test_cases2.length;i++){
        var tcase = test_cases2[i];
        var text = tcase.text, pattern = tcase.pattern;
        info('Searching "'+less(pattern, 20)+'" in "'+less(text, 20)+'"');
        for(var j=0;j<testmethods.length;j++){
            var tmethod = testmethods[j];
            var method = tmethod.method, name = tmethod.name;
            var elapsed = test(method, text.repeat(50), pattern).elapsed;
            stats[name][0] += elapsed;
            if (stats[name][2]<elapsed) stats[name][2] = elapsed;
            if (stats[name][1]>elapsed) stats[name][1] = elapsed;
            msg(name+': '+elapsed+'ms');
        }
        msg(' ');
    }
    console.log();
    info('Total results:');
    var methods = testmethods.map(x=>x.name).sort((a,b)=>stats[a][0]-stats[b][0]);
    for(var i=0;i<methods.length;){
        var method = methods[i];
        var data = stats[method];
        info(++i+') ['+method+'] avg: '+ Math.floor(data[0]/test_cases2.length) 
                + 'ms, max: '+ data[2]+'ms, min:' + data[1]+'ms');
    }
}


function main(){
    if (!EFFICIENCY_ONLY) test_correctness();
    if (!(EFFICIENCY_ONLY ^ CORRECTNESS_ONLY)) console.log();
    if (!CORRECTNESS_ONLY) test_efficiency();
}

main();
