//node.js


function bad_char_shifts(query) {
    var result = []; 
    for(var i=0;i<query.length;i++)
        result[query.charCodeAt(i)] = i+1;
    return result;
}


function spec_equal(pattern, k, l){
    if (k<0) 
        return pattern.substr(0, l-k-1) == pattern.substr(pattern.length-l-k+1);
    return pattern.substr(k, l-1) == pattern.substr(pattern.length-l+1);
}


function good_suffix_shifts(query){
    var result = [];
    if (!query) return [1];
    for(var m=i=query.length; i>=0; i--) {
        for(var k=m-i, rpr=1; k>-i; k--){
            if (k>1 && query[k-1]==query[m-i]) continue;
            if (spec_equal(query, k, i)){
                rpr = k;
                break;
            }
        }
        result.push(m-rpr-i+1);
    }
    return result;
}



function bmloop(query, substring, callback, bshifts, gshifts){
    var length = query.length-(substring.length||1)+1;
    for(var i=0;i<length;){
        var equals = true;
        for(var j=substring.length-1; j>=0; j--)
            if (query[i+j] != substring[j]) {
                equals = false;
                j--;
                break;
            }
        if (equals) callback(i); 
        var gshift = gshifts[++j];
        var bshift = j - 1 - (bshifts[query.charCodeAt(i + j)] || -2);
        i += Math.max(gshift, bshift, 1);
    }
}


module.exports.findall = function(query, substring, callback){
    var bshifts = bad_char_shifts(substring);
    var gshifts = good_suffix_shifts(substring);
    bmloop(query, substring, callback, bshifts, gshifts);
}


module.exports.simplified = {
    'findall' : function(query, substring, callback){
        var bshifts = bad_char_shifts(substring);
        var gshifts = substring ? substring.split('').map(x=>1) : [1];
        bmloop(query, substring, callback, bshifts, gshifts);
    }
}
