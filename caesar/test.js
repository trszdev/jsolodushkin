//node.js
caesar = require('./caesar');
rotate = caesar.rotate, crack = caesar.crack;
alphabet = caesar.alphabet;

function less(text, count) {
    text = text.replace(/[\n\r]/g, '.');
    return text.length < count ? text : text.substr(0, count) + '...';
}


function readtxt(filename){
    return require('fs').readFileSync(filename, 'utf8');
}

// test rotation
var rotate_tests = [
    rotate('It was a typical message from ancient R0m3')==='lafcnRfnfae3tonwfypRRnspfrI0yfn otp afmgyi',
    rotate('lafcnRfnfae3tonwfypRRnspfrI0yfn otp afmgyi', -13)==='It was a typical message from ancient R0m3',
    rotate('Hello world papa', 0)==='Hello world papa',
    rotate(rotate('the Ultimate rotation', 999), -999)==='the Ultimate rotation', 
    rotate('Rotate right then left', 1)===rotate(rotate('Rotate right then left'), -12),
    rotate(rotate('the Legendary rotation', -1000), 1000)==='the Legendary rotation',
    rotate('Simple rotation in specific alphabet', 666, alphabet.standart).indexOf('?')==-1,
    rotate(rotate('I h4v3 $0m3 unkn0wn sуmь01$', 13, alphabet.standart), -13, alphabet.standart)==='I h4v3 0m3 unkn0wn sm01',
    rotate('aaaaaaaaaaa')==='aaaaaaaaaaa',
    rotate('abcdefg', 1)==='gabcdef',
    rotate('абвгдежзий', 1)==='йабвгдежзи',
    rotate('абвгдежзий', -1)==='бвгдежзийа',
].map((x,i) => [i+1, x]);
var mistakes = rotate_tests.filter((x,i)=>!x[1]).map(x=>x[0]);
if (mistakes.length>0) console.log('rotate mistakes: '+mistakes);
else console.log('rotate: ok');
console.log('\n');

var rus1gft = caesar.build_frequency_table(readtxt('rus1.txt'));
var rus2gft = caesar.build_frequency_table(readtxt('rus2.txt'));
var eng1gft = caesar.build_frequency_table(readtxt('eng1.txt'));
var cpp2gft = caesar.build_frequency_table(readtxt('cpp2.txt'));


var crack_tests = [
    { message:'hello world im here and where are you? see you later', rot: 8 },
    { message: 'another c0mmon test of rotation and crack', alphabet: alphabet.standart }, 
    { message:'hello world im here and where are you? see you later', alphabet: alphabet.standart },
    { message:'1\'d 1ik3 2 s3nd 0r1g33n@L freak. teibl' }, 
    { message:'Если ты мне понравишься, можешь звать меня „серж“. Но знаешь что? Ты мне не нравишься! Понятно?', rot: 3, gft: rus1gft },
    { message:"I'm Frankie and yer standing in 'The Hole.' Best damn bar for a hundred miles! Everyone crawls into The Hole when it's time to let loose. Now don't you be causing too much trouble, y'hear?", rot: 7 },
    { message: readtxt('cpp1.txt'), gft: cpp2gft },
    { message: 'This text table take in the lot of t (1 w0u1d h@ck y0ur m3th0d)', gft: eng1gft },
    { message: 'This text table take in the lot of t', gft: eng1gft },
    { message: 'almost elite pen friends will not betray their traditions', gft: eng1gft },
    { message: 'totally leet pen friends wont betray their traditions', gft: eng1gft },
    { message: readtxt('cpp1.txt'), gft: cpp2gft },
    { message: 'используя одну и ту же строку проверю ее на разных таблицах частот забей на пунктуацию', gft: rus1gft },
    { message: 'используя одну и ту же строку проверю ее на разных таблицах частот забей на пунктуацию', gft: rus2gft }
]

LESS_COUNTER = 100;

for(var i=0;i<crack_tests.length;i++){
    var tcase = crack_tests[i];
    var message = tcase.message, rot = tcase.rot,
        alphabet = tcase.alphabet;
    var rotten = rotate(message, rot, alphabet);
    console.log('Original: "'+less(message, LESS_COUNTER)+'"');
    console.log('Rotten:   "'+less(rotten, LESS_COUNTER)+'"');
    var gft = tcase.gft || caesar.build_frequency_table(message);
    console.log('Cracked:  "'+less(crack(rotten, gft, alphabet), LESS_COUNTER)+'"');
    //console.log(gft);
    //console.log(caesar.build_frequency_table(rotten));
    console.log('\n');
}
