//node.js

module.exports.alphabet = {
    standart: 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ _',
    lowercase: 'abcdefghijklmnopqrstuvwxyz',
}

function is_undefined(x) {
    return typeof x == 'undefined';
}

function rotate(query, rot, alphabet){
    if (is_undefined(rot)) rot = 13;
    if (is_undefined(alphabet)) alphabet = grab_alphabet(query);
    rot = -rot;
    if (rot<0) rot += Math.ceil(-rot/alphabet.length)*alphabet.length; 
    rot %= alphabet.length;
    var new_alphabet = alphabet.substr(rot) + alphabet.substr(0, rot);
    return strtr(query, alphabet, new_alphabet);
}


function grab_alphabet(query){
    var result = '';
    var alpha = [];
    for(var i=0;i<query.length;i++) alpha[query.charCodeAt(i)] = 1;
    for(var i=0;i<alpha.length;i++) if (alpha[i]) 
        result += String.fromCharCode(i);
    return result;
}


function strtr(query, from, to) {
    var result = '';
    var convertation = {};
    for(var i=0;i<from.length;i++)
        convertation[from[i]] = to[i];
    for(var i=0;i<query.length;i++) 
        result += convertation[query[i]] || '';
    return result;
}


function build_frequency_table(query){
    var result = {};
    for(var i=0; i<query.length; i++) {
        var temp = query[i];
        if (temp in result) result[temp]++;
        else result[temp] = 1;
    }
    for(var ngram in result)
        result[ngram] /= query.length;
    return result;
}


module.exports.crack = function(query, gft, alphabet){
    if (is_undefined(alphabet)) 
        alphabet = grab_alphabet(query);
    var alphabet2 = alphabet.split('');
    var lft = build_frequency_table(query);
    var mdiff = 2; // dont take care of this
    var mindex = 0;
    for(var i=0;i<alphabet.length;i++) {
        var diff = 0;
        for(var j=0;j<alphabet.length;j++){
            var dletter = Math.abs((lft[alphabet[i]] || 0) - (gft[alphabet2[i]] || 0));
            diff += dletter;
        }
        if (diff<mdiff) {
            mdiff = diff; 
            mindex = i;
        }
        alphabet2.unshift(alphabet2.pop());
    }
    return rotate(query, mindex, alphabet);
}


module.exports.rotate = rotate;
module.exports.build_frequency_table = build_frequency_table;

